// Jquery here
$(document).ready(function() { 
  //Office Tour Slider
	$('.slick-office-thumbs').slick({
		asNavFor: '.slick-office-images',
		dots: false,
		arrows: false,
		infinite: true, 
		slidesToShow: 7,
		slidesToSlide: 7,
		focusOnSelect: true,
		centerMode:true,
		responsive: [
      {breakpoint: 1100,settings:{slidesToScroll:5,slidesToShow: 5}},
      {breakpoint: 600,settings:{slidesToScroll:3,slidesToShow: 3}}
    ]
	});
	
	$('.slick-office-images').slick({
		 slidesToShow: 1,
		 slidesToScroll: 1,
		 arrows: false,
		 fade: true,
		 asNavFor: '.slick-office-thumbs'
	});
	
	//Module 10 Carousel (repurposed here as logo scroll section)
  if($('.slick-m10').length>0){
    var slides = $('.slick-m10').attr('data-slides');    
    $.each( $('.slick-m10'), function(e) {
      var slides = $(this).attr('data-slides');
      $(this).slick({
        infinite: true,
        slidesToShow: parseInt(slides),
        slidesToScroll: 1,
        speed: 400,
        arrows:false,
        dots:false,
        autoplay:true,
        autoplaySpeed:3000,
        responsive: [
          {breakpoint: 1200,settings: {slidesToShow: 3}},
          {breakpoint: 800,settings: {slidesToShow: 2}},
          {breakpoint: 400,settings: {slidesToShow: 1}},
        ]
      });
    });   
  }
});

