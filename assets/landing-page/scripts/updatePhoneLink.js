console.log('Update phone number script is running');

function handleUpdatePhoneLinks() {
  var phoneSpans = document.getElementsByClassName("tracknum");
  var footerLink = document.getElementsByClassName("footer-phone")[0];
  var newNumber = phoneSpans[0].innerText;
  var newNumberLink = 'tel:' + newNumber;

  Array.prototype.forEach.call(document.getElementsByClassName("btn--call-now"), function(button) {
    button.setAttribute("href", newNumberLink);
  });
  
  footerLink.setAttribute("href", newNumberLink);
}

setTimeout(handleUpdatePhoneLinks, 3000);
