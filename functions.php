<?php 
// Require extra meta fields for landing pages v3
if (file_exists( get_stylesheet_directory() . '/lib/landing-page-template-v3-meta.php') ) {
  require_once( get_stylesheet_directory() . '/lib/landing-page-template-v3-meta.php');
}
?>