<?php
	/*
	Template Name: Landing Page 3.1.0 Template 
  */
  
  // Copyright 2018 Dental Revenue | Ads Next / All Rights Reserved
  // Codebase 2 version of the 3.0 Landing page

	// Get all page meta data from this page_id, and save as an object
	$page_id = get_the_ID();
	$page_metas = get_page( $page_id );
	
	// Get the phone number
	if ($page_metas->phone_number) {
		$phone_number = $page_metas->phone_number;
	} else {
		$phone_number = do_shortcode('[new_patient_phone]');
	}

?>
<!doctype html>
<html lang="en-US">
  <head>
	  <?php wp_head(); ?>
    <title><?php wp_title(); ?></title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/style.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <?php
	    // Conditional styling
	    if ($page_metas->background_color || $page_metas->button_color) {
		    echo '<style>';
		    echo set_bg_color_styling($page_metas->background_color);
				echo set_custom_button_color($page_metas->button_color);
				echo '</style>';
			}
		?>
		<script>
			// Get rid of CB2 styles
		  document.addEventListener('DOMContentLoaded', function() {
			  var mainStyle = document.getElementById('main-styles-css');
			  
			  if (mainStyle) {
				  mainStyle.remove();
			  }
		  }); 
	  </script>  
  </head>

  <body>
  
	  <section class="solid-color-background fixed <?php if ($page_metas->sticky_header_enabled == 'true') { echo 'sticky-header'; } ?>">
	    <header class="flex vertically-center">
	      <div id="logo">
	        <img src="<?php echo site_ops_logo(); ?>" alt="<?php get_the_h1(); ?>" />
	      </div>
	
	      <div class="cta-buttons flex">
	        <div class="btn">
	          <a class="btn--call-now" href=""><i class="fas fa-phone rotated"></i><span class="tracknum"><?php echo $phone_number; ?></span></a>
	        </div>
	
	        <div class="btn">
		        <?php $page = get_pages(array('meta_key' => '_wp_page_template','meta_value' => 'page-templates/template-schedule.php')); ?>
	          <a class="btn--schedule-apt" href="<?php echo get_permalink($page[0]->ID); ?>">
	            <i class="fas fa-calendar-plus"></i>
	            Schedule An Appointment
	          </a>
	        </div>
	      </div>
	    </header>
		</section>	
					
    <section class="hero <?php if ($page_metas->sticky_header_enabled == 'true') { echo 'sticky-header-hero'; } ?>" style="background: url(<?php echo $page_metas->hero_background_image; ?>) no-repeat;background-position: center <?php echo $page_metas->hero_img_alignment; ?>;background-size: cover;min-height: 550px;position: relative;">
      <p><?php echo $page_metas->hero_slogan; ?></p>
    </section>

    <section class="solid-color-background">
      <div class="headline container">
        <h1><?php echo $page_metas->h1_headline; ?></h1>
      </div>
    </section>
	    
    <?php 
	    // Generate <section> elements via the repeatable fields,
	    // using template partials 
	    $sections = $page_metas->sections; 
	    
	    foreach ((array) $sections as $section) {
		    // Use a normal info section partial for Meet the Dr. sections, but allow it to  
		    // display as a 'Meet the Dr.' section in the admin panel
		    $section_type = ($section['section_type'] == 'meet-the-dr') ? 'info-section' : $section['section_type'];
		    
				include(locate_template('page-templates/landing-page-partials/' . $section_type . '.php'));
			}
	  ?>
	    
  <footer>
    <section class="grey-background">
      <h2 id="location" class="centered">Conveniently located in <?php site_ops_city(); ?>, <?php site_ops_state(); ?></h2>
      <div class="flex container">
        <div class="info" id="google-map-container">
          <?php site_ops_google_map(); ?>
        </div>
        <div class="contact-column" id="contact-info">
          <h3>Contact</h3>
          <p><strong><?php site_ops_practice_name(); ?></strong></p>
          <p><?php site_ops_address(); ?></p>
          <p><?php site_ops_city(); ?>, <?php site_ops_state(); ?> <?php site_ops_zip(); ?></p>
          
          <h3>New Patients</h3>
          <p>
          <a class="footer-phone">
            <i class="fas fa-phone rotated"></i><span class="tracknum"><?php echo $phone_number; ?></span>
          </a>
          </p>
        </div>
        <div class="contact-column" id="hours">
          <h3>Hours</h3>
          <dl>
            <dt>Monday</dt>
            <dd>8am – 4pm</dd>
            <dt>Tuesday</dt>
            <dd>8am – 4pm</dd>
            <dt>Wednesday</dt>
            <dd>8am – 4pm</dd>
            <dt>Thursday</dt>
            <dd>8am – 4pm</dd>
            <dt>Friday - Sunday</dt>
            <dd>Closed</dd>
          </dl>
        </div>
        
      </div>
    </section>
  </footer>
  <?php wp_footer() ?>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/scripts/slick.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/scripts/drgallery2.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/scripts/jquery.magnific-popup.min.js"></script>
  <script defer src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/scripts/updatePhoneLink.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/scripts/jQueryScripts.js"></script>
  </body>
</html> 