<section>
  <div class="flex container">
    <div class="info text-box" id="payment">
      <h2><?php echo $section['section_headline']; ?></h2>
      
      <div id="payment-copy">
      <?php
				echo wpautop( $section['section_content'] );
			?>
      </div>
    </div>
  </div>
</section>