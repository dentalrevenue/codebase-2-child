<section class="container office-tour">
	<h2><?php echo $section['section_headline']; ?></h2>
	<div class="ds-row">
			<?php $images = $section['custom_gallery_images']; ?>
      
      <div class="slick-office-thumbs">
        <?php foreach ( (array) $images as $image ) { ?>
					<div class="office-thumb plus-parent custom-gallery-thumb" style="background-image: url(<?php echo $image; ?>)"></div>
  		  <?php } ?>
      </div>

      <div class="slick-office-images">
        <?php foreach ( (array) $images as $image ) { ?>
					<div class="file-list-image"><img src="<?php echo $image; ?>" /></div>
  		  <?php } ?>
      </div>
	  </div>                

	</div>
</section>