<?php $columns = $page_metas->three_columns; ?>

<section class="three-columns">
	<h2><?php echo $section['section_headline']; ?></h2>
	
	<div class="container">
		
		<?php foreach ( (array) $columns as $column) { ?>
			<div class="third-column">
				<h3><?php echo $column['title']; ?></h3>
				
				<?php echo wpautop( $column['content'] ); ?>
				
			</div>
			
		<?php } ?>
		
	</div>
</section>