<section class="grey-background video">
  <h2 id="video-title" class="centered"><?php echo $section['section_headline']; ?></h2>
  <div class="flex container">
    <?php
      $video_url = esc_url($section['section_video']);
      echo wp_oembed_get( $video_url );
    ?>
  </div>
</section>