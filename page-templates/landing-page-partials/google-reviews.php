<section>
	<h2 id="testimonials" class="centered"><?php echo $section['section_headline']; ?></h2>
	
	<div class="flex wide-container no-padding">
	  
	  <?php
	    
	    $practice_info = getOption('practice_info');
		  $place_id      = $practice_info['google_place_id'];

			if(!$place_id || $place_id==''){
				echo "<h1>Set Place ID in Theme Settings</h1>";
			}else{
			
				$jsonurl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=$place_id&key=AIzaSyD7dIa6edUzrhrYIrwXDab2B0nfF56om5w";
				$json = file_get_contents($jsonurl);
				$json = json_decode($json);
				
				$reviews = $json->result->reviews;
				$counter = 0;
				
				foreach ($reviews as $review){
					if($review->rating>4){
						$counter++;
						$patient_name = $review->author_name;
						$patient_review_url = $review->author_url;
						$patient_image = $review->profile_photo_url;
						$patient_review = $review->text; ?>
	
						<div class="info">
			        
			        <div class="testimonial-person-info">
			          
			          <div class="centered">
			          	<a href="<?php echo $patient_review_url; ?>" target="_blank">
		            		<img src="<?php echo $patient_image;?>" class="img--testimonial">
			          	</a>
			          	<a href="<?php echo $patient_review_url; ?>" target="_blank">
				          	<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/images/google-logo-150.png" class="google-logo" />
				        	</a>
			          </div>
			          
			          <h3 class="review-name">
					          <?php echo $patient_name; ?>
					        </a>
			          </h3>  
		          </div>
			        
		          <p class="g-customer-excerpt"><?php echo wp_trim_words($patient_review,60); ?></p>
			          
		            <div class="google-stars">
			            <i class="fas fa-star"></i>
			            <i class="fas fa-star"></i>
			            <i class="fas fa-star"></i>
			            <i class="fas fa-star"></i>
			            <i class="fas fa-star"></i>
		          	</div>
		          	
		          	<p class="five-stars">5 out of 5 stars</p>
		        </div>
						
					<?php }
					if($counter>2){ break; }
				}
			} ?>
	  </div>
	</section>