<?php 
  //dev note: all association images must be 400x168px
  $num_slides = 4; ?>

<section class="container">
  
  <div class="row">
    <div class="slick-init slick-m10" data-slides="<?php echo $num_slides; ?>">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/images/assoc-aacd.jpg" alt="AACD" />
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/images/assoc-agd.jpg" alt="American Dental Association" />
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/images/assoc-cerec.jpg" alt="CEREC Same Day Crowns" />
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/landing-page/images/assoc-invisalign.jpg" alt="Academy of General Dentistry" />
    </div>
  </div>
    	
</section>