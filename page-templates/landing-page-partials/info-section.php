<?php 
	$alignment = $section['section_content_alignment'];
?>
<section class="info-section">
  <div class="flex container"> 
    <div class="info text-box <?php echo $alignment; ?>">
      <h2><?php echo $section['section_headline']; ?></h2>
			<?php
				// Auto wrap wysiwyg content in <p> tags 
				echo wpautop( $section['section_content'] );
			?> 
    </div>
    <div class="info img-box <?php echo $alignment; ?>" style="background-image: url(<?php echo $section['section_image']; ?>);">
    	<?php if ($section['section_video']) {
        // Display a Youtube video if one is provided
        ?> 
        	<?php 
	        	$url = esc_url($section['section_video']);
	        	echo wp_oembed_get( $url ); ?>
      <?php } else {
        // Otherwise, display an image
        ?>
        	<img src="<?php echo $section['section_image']; ?>" class="img--info" alt="">
      <?php } ?>
    </div>
  </div>
</section>