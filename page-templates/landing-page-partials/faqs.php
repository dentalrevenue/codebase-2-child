<?php $faqs = $page_metas->faqs; ?>
				 
<section id="faqs">
	<h2>Frequently Asked Questions</h2>
	<div class="container">
		<?php foreach( (array) $faqs as $faq) {
		$question = $answer = '';
		 
			 if ( isset( $faq['question'])) {
				 $question = $faq['question'];
			 }
			 
			 if ( isset( $faq['answer'])) {
				 $answer = $faq['answer'];
			 }
			 
			 ?>
			 <div class="faq">
				 <h3><?php echo $question; ?></h3>
				 <p><?php echo $answer; ?></p>
			 </div>
		<?php } // end FAQs loop ?> 
  </div>
</section>