<?php $videos = $page_metas->video_testimonials; ?>

<section class="video-testimonials">
	<h2><?php echo $section['section_headline']; ?></h2>
	<div class="container">
		
			<?php foreach ( (array) $videos as $video) { ?>
			<div class="video-column">
				<div class="video-wrapper">
					<iframe src="<?php echo $video['video_link']; ?>" allow="encrypted-media"></iframe>
				</div>
				
				<h3><?php echo $video['video_title']; ?></h3>
			</div>
			<?php	} ?>
		</div>
	</div>
</section>