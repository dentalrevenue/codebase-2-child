<?php 

function main_page_settings() {
  $box = new_cmb2_box(array(
    'id' => 'main_page_settings',
    'title' => __( 'Main Content Settings', 'cmb2' ),
    'object_types' => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'page-templates/template-landing-page-v3.php' ),
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));
  $box->add_field( array( 
    'name' => 'Default Phone Number',
    'desc' => 'The default phone number for Call Now buttons (it may be changed dynamically later). Leave blank to default to this site\'s New Patient phone number.',
    'id' => 'phone_number',
    'type' => 'text',
  ));
  $box->add_field( array(
    'name' => '"Sticky" Header',
    'desc' => 'Choose whether the header should stick to the top of the screen<br> and follow the viewer down the page (enabled by default)',
    'id' => 'sticky_header_enabled',
    'type' => 'radio',
    'options' => array(
	    'true'  => __('Enabled', 'cmb2'),
	    'false' => __('Disabled', 'cmb2'),
    ),
    'default'   => 'true',
  ));
  $box->add_field( array( 
    'name' => 'Page Headline',
    'desc' => 'The H1 Tag',
    'id' => 'h1_headline',
    'type' => 'text'
  ));
  $box->add_field( array( 
    'name' => 'Hero slogan',
    'desc' => 'Hero slogan (text that overlays hero image)',
    'id' => 'hero_slogan',
    'type' => 'text'
  ));
  $box->add_field( array( 
    'name' => 'Hero Background Image',
    'desc' => 'Hero Background Image',
    'id' => 'hero_background_image',
    'type' => 'file',
    'query_args' => array( 
      'type' => array( 
        'image/png', 
        'image/jpeg' 
        ), 
      ), 
      'preview_size' => 'medium',
  ));
	$box->add_field( array( 
    'name' => 'Hero Background Image Alignment',
    'desc' => 'Select the Top or Bottom to make sure that part of the image is always visible, or Center if the image focus is right in the center',
    'id' => 'hero_img_alignment',
    'type' => 'select',
    'default' => 'center',
    'options'          => array(
			'top' => __( 'Top', 'cmb2' ),
			'center'   => __( 'Center', 'cmb2' ),
			'bottom'     => __( 'Bottom', 'cmb2' ),
		),
  ));
}

function sections() {
	$box = new_cmb2_box(array(
    'id' => 'sections',
    'title' => __( 'Repeatable & Arrangeable Content Sections', 'cmb2' ),
    'object_types' => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'page-templates/template-landing-page-v3.php' ),
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));
	$sections = $box->add_field( array(
	    'id'          => 'sections',
	    'type'        => 'group',
	    'description' => __( 'Add as many Sections as you want, and use the ^ / ⌄ buttons (on the bottom left) to re-order sections.<br>Section Type Instructions:<br>"Image & Text Info": Subheadline and text on one side, image on the other. Choose image on the left or right.<br>- "Breaker Headline": adds a headline w/solid color background. Fill out just the the "headline" field.<br>- "Full Width Video": adds a full width video. Just fill out the headline and video fields for this section.<br>- "Meet the Doctor": the same layout and fields as an info section, but marked "Meet the Dr." in the admin panel.<br>- "FAQs": activates the FAQs section. Please add/edit/remove FAQs in the FAQs box below. Leave fields blank.<br>- "Logo Scroll": activates the Logo Scroll section and loads some default logos (a developer will need to add or edit these). Leave fields blank.<br>- "Payment & Insurance Info": A single, larger, bordered box for Payment Info. Use just the headline and the Section Content field.<br>- "YouTube Video Testimonials": Add up to 3 YouTube links via the YouTube videos panel below and they will populate this section<br>- "Custom Gallery": Add as many images as desired and they will be displayed in an office-tour or smile-gallery style slider.<br>- "Three Columns: Add content via the "Three Columns" section below. This section will render these columns 3 across', 'cmb2'),
	    'options'     => array(
		    'group_title'   => __( 'Section {#}', 'cmb2'), // {#} gets replaced by row number
		    'add_button'    => __( 'Add Another Section', 'cmb2'),
		    'remove_button' => __( 'Remove This Section', 'cmb2'),
		    'sortable'      => true,
	    ),
    ));
  $box->add_group_field( $sections, array(
    'name' => 'Section Type',
    'description' => 'The type of section',
    'id' => 'section_type',
    'type' => 'select',
			'default'  => 'custom',
			'options'          => array(
				'info-section' => __( 'Image & Text Info Section', 'cmb2' ),
				'breaker-headline'   => __( 'Breaker Headline', 'cmb2' ),
				'full-width-video' =>   __( 'Full Width Video Section', 'cmb' ),
				'meet-the-dr'     => __( 'Meet the Doctor', 'cmb2' ),
				'google-reviews'  => __('Google Reviews', 'cmb2'),
				'faqs'     => __( 'FAQs', 'cmb2' ),
				'logo-scroll'     => __( 'Logo Scroll', 'cmb2' ),
        'payment'         => __( 'Payment & Insurance Info', 'cmb2' ),
        'video-testimonials'         => __( 'YouTube Video Testimonials', 'cmb2' ),
        'custom-gallery'         => __( 'Custom Gallery', 'cmb2' ),
        'three-columns'         => __( 'Three Columns', 'cmb2' ),
			),
  ));
  $box->add_group_field( $sections, array(
    'name' => 'Section Headline',
    'description' => 'The H3 subhead for this section.',
    'id' => 'section_headline',
    'type' => 'text',
  ));
    $box->add_group_field( $sections, array(
    'name' => 'Section Image',
    'description' => 'An image to accompany the info content.',
    'id' => 'section_image',
    'type' => 'file',
      'query_args' => array( 
        'type' => array( 
          'image/png', 
          'image/jpeg', 
          ), 
        ), 
    'preview_size' => 'medium',
  ));
  $box->add_group_field( $sections, array(
    'name' => 'Section Video',
    'description' => 'A video to accompany the info content, or for a full-width video section. (Replaces any image selected above)',
    'id' => 'section_video',
    'type' => 'oembed',
  ));
  $box->add_group_field( $sections, array(
		'name' => 'Custom Gallery (only displayed on "Custom Gallery" sections)',
		'desc' => 'Please select "Custom Gallery" from the section types',
		'id'   => 'custom_gallery_images',
		'type' => 'file_list',
	));
  $box->add_group_field( $sections, array(
    'name' => 'Section Content',
    'description' => 'The content',
    'id' => 'section_content',
    'type' => 'wysiwyg',
  ));
  $box->add_group_field( $sections, array(
    'name' => 'Image / Content Alignment',
    'description' => 'Select how you want the image/video and content laid out',
    'id' => 'section_content_alignment',
    'type' => 'radio',
    'options' => array(
	    'img-left'  => __('<strong>Image/video</strong> on the left, content on the right', 'cmb2'),
	    'img-right' => __('<strong>Content</strong> on the left, image/video on the right', 'cmb2'),
    ),
  ));
}

function faqs() {
	$box = new_cmb2_box(array(
    'id' => 'faqs',
    'title' => __( 'The FAQs section - select "FAQs" to display and arrange this section above.', 'cmb2' ),
    'object_types' => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'page-templates/template-landing-page-v3.php' ),
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));
  $faqs = $box->add_field( array(
    'id'          => 'faqs',
    'type'        => 'group',
    'description' => __( 'Add as many FAQs as you want', 'cmb2'),
    'options'     => array(
	    'group_title'   => __( 'FAQ {#}', 'cmb2'), // {#} gets replaced by row number
	    'add_button'    => __( 'Add Another FAQ', 'cmb2'),
	    'remove_button' => __( 'Remove This FAQ', 'cmb2'),
	    'sortable'      => true,
    ),
  ));
  $box->add_group_field( $faqs, array(
    'name' => 'Question / Title of FAQ',
    'description' => 'Write a short question about this dental service or product',
    'id' => 'question',
    'type' => 'text',
  ));
  $box->add_group_field( $faqs, array(
    'name' => 'Answer / FAQ information',
    'description' => 'Provide the answer to the FAQ here',
    'id' => 'answer',
    'type' => 'textarea_small',
  ));
}

function video_testimonials() {
	$box = new_cmb2_box(array(
    'id' => 'video_testimonials',
    'title' => __( 'YouTube video testimonials - select "YouTube Testimonials section" to display this section above.', 'cmb2' ),
    'object_types' => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'page-templates/template-landing-page-v3.php' ),
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));
  $faqs = $box->add_field( array(
    'id'          => 'video_testimonials',
    'type'        => 'group',
    'description' => __( 'Add up to 3 YouTube videos', 'cmb2'),
    'options'     => array(
	    'group_title'   => __( 'YouTube Video {#}', 'cmb2'), // {#} gets replaced by row number
	    'add_button'    => __( 'Add Another Video', 'cmb2'),
	    'remove_button' => __( 'Remove This Video', 'cmb2'),
	    'sortable'      => true,
    ),
  ));
  $box->add_group_field( $faqs, array(
    'name' => 'Video Link',
    'description' => 'Paste the link to the YouTube video - must be formatted like https://www.youtube.com/embed/oNna57WEYmc',
    'id' => 'video_link',
    'type' => 'oembed',
  ));
  $box->add_group_field( $faqs, array(
    'name' => 'Title',
    'description' => 'Provide a title for the video (optional)',
    'id' => 'video_title',
    'type' => 'text',
  ));
}

function three_columns() {
	$box = new_cmb2_box(array(
    'id' => 'three_columns',
    'title' => __( 'Content Sections that render in a 3 columns across layout', 'cmb2' ),
    'object_types' => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'page-templates/template-landing-page-v3.php' ),
    'context' => 'normal',
    'priority' => 'high',
    'show_names' => true,
  ));
  $faqs = $box->add_field( array(
    'id'          => 'three_columns',
    'type'        => 'group',
    'description' => __( 'Add up to 3+ columns', 'cmb2'),
    'options'     => array(
	    'group_title'   => __( 'Text Column {#}', 'cmb2'), // {#} gets replaced by row number
	    'add_button'    => __( 'Add Another Column', 'cmb2'),
	    'remove_button' => __( 'Remove This Column', 'cmb2'),
	    'sortable'      => true,
    ),
  ));
  $box->add_group_field( $faqs, array(
    'name' => 'Headline',
    'description' => 'The title',
    'id' => 'title',
    'type' => 'text',
  ));
  $box->add_group_field( $faqs, array(
    'name' => 'Content',
    'description' => 'The content',
    'id' => 'content',
    'type' => 'wysiwyg',
  ));
}

add_action( 'cmb2_admin_init', 'main_page_settings' );

add_action( 'cmb2_admin_init', 'sections' );

add_action( 'cmb2_admin_init', 'faqs' );

add_action( 'cmb2_admin_init', 'video_testimonials' );

add_action( 'cmb2_admin_init', 'three_columns' );

// Replace repeatable section #'s with Section types for easier editing
// Source: https://github.com/CMB2/CMB2-Snippet-Library/blob/master/javascript/dynamically-change-group-field-title-from-subfield.php

function add_js_to_admin_footer() {
	add_action( 'admin_footer', 'add_js_for_repeatable_titles_delete_confirm_to_footer' );
}

function add_js_for_repeatable_titles_delete_confirm_to_footer() {
	?>
	<script type="text/javascript">
	jQuery( function( $ ) {
		var $box = $( document.getElementById( 'sections' ) );
		var replaceTitles = function() {
			$box.find( '.cmb-group-title' ).each( function() {
				var $this = $( this );
				var txt = $this.next().find( '[id$="section_type"]' ).val();
				var rowindex;
				if ( ! txt ) {
					txt = $box.find( '[data-grouptitle]' ).data( 'grouptitle' );
					if ( txt ) {
						rowindex = $this.parents( '[data-iterator]' ).data( 'iterator' );
						txt = txt.replace( '{#}', ( rowindex + 1 ) );
					}
				}
				if ( txt ) {
					$this.text( txt );
				}
			});
		};
		var replaceOnKeyUp = function( evt ) {
			var $this = $( evt.target );
			var id = 'title';
			if ( evt.target.id.indexOf(id, evt.target.id.length - id.length) !== -1 ) {
				$this.parents( '.cmb-row.cmb-repeatable-grouping' ).find( '.cmb-group-title' ).text( $this.val() );
			}
		};
		$box
			.on( 'cmb2_add_row cmb2_shift_rows_complete', replaceTitles )
			.on( 'keyup', replaceOnKeyUp );
		replaceTitles();
	
		// Alert for deleting sections
		$('.cmb-remove-group-row').click(function(e) {
			e.preventDefault();
			
			var ok = confirm('WARNING: Deleting this section cannot be undone! Are you sure?');
			
			if (ok) {
				this.submit;
			} else {
				return false;
			}
		});
	});
	
	
	</script>
	<?php
}

add_js_to_admin_footer();
?>